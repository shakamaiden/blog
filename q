BUNDLE(1)                                                                                                  BUNDLE(1)

NNAAMMEE
       bbuunnddllee - Ruby Dependency Management

SSYYNNOOPPSSIISS
       bbuunnddllee COMMAND [--no-color] [--verbose] [ARGS]

DDEESSCCRRIIPPTTIIOONN
       Bundler manages an aapppplliiccaattiioonn´´ss ddeeppeennddeenncciieess through its entire life across many machines systematically and
       repeatably.

       See the bundler website _h_t_t_p_:_/_/_b_u_n_d_l_e_r_._i_o for information on getting started, and Gemfile(5) for more  infor‐
       mation on the GGeemmffiillee format.

OOPPTTIIOONNSS
       ----nnoo--ccoolloorr
              Print all output without color

       ----vveerrbboossee
              Print out additional logging information

BBUUNNDDLLEE CCOOMMMMAANNDDSS
       We divide bbuunnddllee subcommands into primary commands and utilities.

PPRRIIMMAARRYY CCOOMMMMAANNDDSS
       bbuunnddllee iinnssttaallll((11)) _b_u_n_d_l_e_-_i_n_s_t_a_l_l_._1_._h_t_m_l
              Install the gems specified by the GGeemmffiillee or GGeemmffiillee..lloocckk

       bbuunnddllee uuppddaattee((11)) _b_u_n_d_l_e_-_u_p_d_a_t_e_._1_._h_t_m_l
              Update dependencies to their latest versions

       bbuunnddllee ppaacckkaaggee((11)) _b_u_n_d_l_e_-_p_a_c_k_a_g_e_._1_._h_t_m_l
              Package the .gem files required by your application into the vveennddoorr//ccaacchhee directory

       bbuunnddllee eexxeecc((11)) _b_u_n_d_l_e_-_e_x_e_c_._1_._h_t_m_l
              Execute a script in the context of the current bundle

       bbuunnddllee ccoonnffiigg((11)) _b_u_n_d_l_e_-_c_o_n_f_i_g_._1_._h_t_m_l
              Specify and read configuration options for bundler

       bbuunnddllee hheellpp((11))
              Display detailed help for each subcommand

UUTTIILLIITTIIEESS
       bbuunnddllee cchheecckk((11))
              Determine whether the requirements for your application are installed and available to bundler

       bbuunnddllee sshhooww((11))
              Show the source location of a particular gem in the bundle

       [bbuunnddllee oouuttddaatteedd((11))][bundle-outdated]
              Show all of the outdated gems in the current bundle

       bbuunnddllee ccoonnssoollee((11))
              Start an IRB session in the context of the current bundle

       bbuunnddllee ooppeenn((11))
              Open an installed gem in the editor

       [bbuunnddllee lloocckk((11))][bundle-lock]
              Generate a lockfile for your dependencies

       bbuunnddllee vviizz((11))
              Generate a visual representation of your dependencies

       bbuunnddllee iinniitt((11))
              Generate a simple GGeemmffiillee, placed in the current directory

       bbuunnddllee ggeemm((11)) _b_u_n_d_l_e_-_g_e_m_._1_._h_t_m_l
              Create a simple gem, suitable for development with bundler

       bbuunnddllee ppllaattffoorrmm((11)) _b_u_n_d_l_e_-_p_l_a_t_f_o_r_m_._1_._h_t_m_l
              Display platform compatibility information

       bbuunnddllee cclleeaann((11))
              Clean up unused gems in your bundler directory

       bbuunnddllee ddooccttoorr((11))
              Display warnings about common potential problems

PPLLUUGGIINNSS
       When  running  a command that isn´t listed in PRIMARY COMMANDS or UTILITIES, Bundler will try to find an exe‐
       cutable on your path named bbuunnddlleerr--<<ccoommmmaanndd>> and execute it, passing down any extra arguments to it.

BBUUNNDDLLEERR TTRRAAMMPPOOLLIINNIINNGG
       Bundler includes a feature called trampolining, designed to allow a single  developer  to  work  on  multiple
       projects, each on different Bundler versions. The trampoline will infer the correct version of Bundler to use
       for each project and load that version instead of the version directly invoked (which is  almost  always  the
       newest version installed locally).

       Bundler  by default will use the Bundler version in the current directory to determine the version to trampo‐
       line to, reading from the BBUUNNDDLLEEDD WWIITTHH section. However, if the BBUUNNDDLLEERR__VVEERRSSIIOONN environment variable is  set,
       that version will override the lockfile inference and can be used in directories without a lockfile.

       Until  the target version is Bundler 2 or later, BBUUNNDDLLEE__TTRRAAMMPPOOLLIINNEE__FFOORRCCEE must be set for the trampoline to be
       used. Additionally, BBUUNNDDLLEE__TTRRAAMMPPOOLLIINNEE__DDIISSAABBLLEE can be set to completely disable the trampoline.

OOBBSSOOLLEETTEE
       These commands are obsolete and should no longer be used

       ·   bbuunnddllee ccaacchhee((11))

       ·   bbuunnddllee lliisstt((11))

                                                    February 2017                                          BUNDLE(1)
