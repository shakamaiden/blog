Rails.application.routes.draw do
  get 'welcome/index'
  get 'movieapi/index'

	resources :articles do
	  resources :comments
	end

  root 'movieapi#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
